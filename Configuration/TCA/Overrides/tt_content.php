<?php
defined('TYPO3') or die('Access denied.');

(function($table) {


    // Extra fields for the tt_content table
    $extraContentColumns = [
       'image_responsive' => [
           'exclude' => 0,
           'l10n_mode' => 'exclude',
           'label' => 'Simple Responsive Image',
           'config' => [
               'type' => 'check',
           ],
       ],
        'tx_cogtail_wrap_id_input' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_id_input',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim,nospace',
            ],
        ],
        'topheader' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'Topheader',
            'config' => [
                'type' => 'input',
                'size' => 100
            ],
        ],
        'additionalheader' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_class_additional_header',
            'config' => [
                'type' => 'input',
                'size' => 100
            ],
        ],
        'tx_cogtail_wrap_noheader' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'No Header',
            'config' => [
                'type' => 'check',
                'items' => [
                   ['No <header>', ''],
                ],
            ],
        ],
        'tx_cogtail_wrap_class_input' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_class_input',
            // 'onChange' => 'reload',
            'description' => 'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_class_input_desc',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectCheckBox',
              'items' => [
                [
                    'Hero',
                    'hero-content'
                ],
                [
                    'Feature',
                    'feature-content'
                ],
                [
                    'Alert',
                    'alert'
                ],
                [
                    'Panel',
                    'panel'
                ],
                [
                    'Avatar',
                    'avatar'
                ],
                [
                    'Action',
                    'action'
                ],
                [
                    'Testimonial',
                    'testimonial'
                ],
                [
                    'Gallery/Image',
                    'gallery'
                ],
                [
                    'Header',
                    'header'
                ],
                [
                    'Content',
                    'content'
                ],
                [
                    'Grid/Container',
                    'grid-container'
                ],
                [
                    'Table',
                    'table'
                ],
                [
                    'List',
                    'listing'
                ],
                [
                    'Reset',
                    'reset'
                ],
                [
                    'Mobile',
                    'mobile'
                ],
                [
                    'Tablet',
                    'tablet'
                ],
                [
                    'Desktop',
                    'desktop'
                ]
              ]
          ],
        ],
        'tx_cogtail_wrap_class_props' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_class_wechsler',
            'onChange' => 'reload',
            # 'description' => 'This is a test description',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'default' => 1,
              'items' => [
                  ['LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_class_wechsler_layouts', 0],
                  ['LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_class_wechsler_properties', 1],
                  ['LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_class_wechsler_special', 2]
              ]
          ],
        ],
        'tx_cogtail_wrap_class_style' => [
            'exclude' => 1,
            'displayCond' => [
              'AND' => [
                'FIELD:tx_cogtail_wrap_class_props:!=:1',
                'FIELD:tx_cogtail_wrap_class_props:!=:2',
              ],
            ],
            'l10n_mode' => 'exclude',
            'label' => 'Layouts',
            'description' => 'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_class_layouts_desc',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectCheckBox',
              'items' => [
                [
                    'LLL:EXT:cogtail_publishing_child/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_class_layouts_1',
                    'theme-1'
                ],
                [
                    'LLL:EXT:cogtail_publishing_child/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_class_layouts_2',
                    'theme-2'
                ],
                [
                    'LLL:EXT:cogtail_publishing_child/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_class_layouts_3',
                    'theme-3'
                ],
                [
                    'LLL:EXT:cogtail_publishing_child/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_class_layouts_4',
                    'theme-4'
                ],
                [
                    'LLL:EXT:cogtail_publishing_child/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_class_layouts_5',
                    'theme-5'
                ],
                [
                    'LLL:EXT:cogtail_publishing_child/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_class_layouts_6',
                    'theme-6'
                ]
              ]
          ],
        ],
        'tx_cogtail_wrap_icons' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'Header-Icons & -Border',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  ['Info', 'foo1', 'EXT:cogtail_wrap/Resources/Public/Icons/info.svg'],
                  ['Phone', 'foo2', 'EXT:cogtail_wrap/Resources/Public/Icons/phone.svg'],
                  ['Heart', 'foo3', 'EXT:cogtail_wrap/Resources/Public/Icons/heart.svg'],
                  ['Envelope', 'foo4', 'EXT:cogtail_wrap/Resources/Public/Icons/envelope.svg'],
                  ['Marker', 'foo5', 'EXT:cogtail_wrap/Resources/Public/Icons/marker.svg'],
                  ['User', 'foo6', 'EXT:cogtail_wrap/Resources/Public/Icons/user-friends.svg'],
                  ['Chevron', 'foo7', 'EXT:cogtail_wrap/Resources/Public/Icons/chevron-right.svg'],
                  ['Angle', 'foo8', 'EXT:cogtail_wrap/Resources/Public/Icons/angle-right.svg'],
                  ['Simple border', 'foo9'],
              ]
          ],
        ],
        'tx_cogtail_wrap_input30' => [
            'exclude' => 1,
            'label' => 'H Size',
            'config' => [
                'type' => 'input',
                'size' => 5,
                'eval' => 'trim,int',
                'range' => [
                    'lower' => 0,
                    'upper' => 96,
                ],
                'default' => 0,
                'slider' => [
                    'step' => 2,
                    'width' => 200,
                ],
            ],
        ],
        'tx_cogtail_wrap_lheight' => [
          'exclude' => 1,
          'l10n_mode' => 'exclude',
          'label' => 'Line Height',
          'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                ['', 0],
                [
                    '1',
                    'lh-1'
                ],
                [
                    '1.1',
                    'lh-1-1'
                ],
                [
                    '1.2',
                    'lh-1-2'
                ],
                [
                    '1.3',
                    'lh-1-3'
                ],
                [
                    '1.4',
                    'lh-1-4'
                ],
                [
                    '1.5',
                    'lh-1-5'
                ],
                [
                    '1.6',
                    'lh-1-6'
                ],
                [
                    '1.7',
                    'lh-1-7'
                ],
                [
                    '1.8',
                    'lh-1-8'
                ]
            ]
          ],
        ],
        'tx_cogtail_wrap_hweight' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'Font Weight',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                ['', 0],
                [
                  'Light',
                  'fw-light'
                ],
                [
                  'Normal',
                  'fw-normal'
                ],
                [
                  'Bold',
                  'fw-bold'
                ]
            ]
          ],
        ],
        'tx_cogtail_wrap_hdisplay' => [
          'exclude' => 1,
          'label' => 'Display',
        //  'onChange' => 'reload',
          'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      'Display 1',
                      'display-1',
                  ],
                  [
                      'Display 2',
                      'display-2',
                  ],
                  [
                      'Display 3',
                      'display-3',
                  ],
                  [
                      'Display 4',
                      'display-4',
                  ],
                  [
                      'Display 5',
                      'display-5',
                  ],
                  [
                      'Display 6',
                      'display-6',
                  ]
              ],
          ],
      ],
        'tx_cogtail_wrap_hlayout' => [
            'exclude' => 1,
            // https://jweiland.net/typo3/beispiele-anleitungen-faq/extension-programmierung/displaycond.html
            // 'displayCond' => 'FIELD:tx_cogtail_wrap_hdisplay:=:1',
            //'displayCond' => 'FIELD:CType:=:textpic',
            'l10n_mode' => 'exclude',
            'label' => 'Header Layout',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                ['', 0],
                [
                  'Uppercase',
                  'h-uppercase'
                ],
                [
                  'Normal',
                  'h-normal'
                ],
                [
                  'Letter Spacing',
                  'h-letter-spacing'
                ],
                [
                  'Small',
                  'h-small'
                ],
                [
                  'Layout 5',
                  'display-5'
                ],
                [
                  'Layout 6',
                  'hlayout-6'
                ]
            ]
          ],
        ],
        'tx_cogtail_wrap_hwidth' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'Width',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                ['', 0],
                [
                  'Max Width Large',
                  'mw-75'
                ],
                [
                  'Max Width Medium',
                  'mw-50'
                ],
                [
                  'Max Width Small',
                  'mw-25'
                ],
                [
                  'Min Width Large',
                  'w-75'
                ],
                [
                  'Min Width Medium',
                  'w-50'
                ],
                [
                  'Min Width Small',
                  'w-25'
                ]
            ]
          ],
        ],
        'tx_cogtail_wrap_hcolor' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'Font Color',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      'Primary',
                      'fc-primary'
                  ],
                  [
                      'Primary Variant',
                      'fc-primary-var'
                  ],
                  [
                      'Secondary',
                      'fc-secondary'
                  ],
                  [
                      'Accent',
                      'fc-accent'
                  ],
                  [
                      'White',
                      'fc-white'
                  ],
                  [
                      'Light',
                      'fc-light'
                  ],
                  [
                      'Dark',
                      'fc-dark'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_hstyles' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'Font Size',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      'H1 Style',
                      'h1'
                  ],
                  [
                      'H2 Style',
                      'h2'
                  ],
                  [
                      'H3 Style',
                      'h3'
                  ],
                  [
                      'H4 Style',
                      'h4'
                  ],
                  [
                      'H5 Style',
                      'h5'
                  ],
                  [
                      'H6 Style',
                      'h6'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_bfontsize' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'Font Size',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      'Font Size 1',
                      'fs-1'
                  ],
                  [
                      'Font Size 2',
                      'fs-2'
                  ],
                  [
                      'Font Size 3',
                      'fs-3'
                  ],
                  [
                      'Font Size 4',
                      'fs-4'
                  ],
                  [
                      'Font Size 5',
                      'fs-5'
                  ],
                  [
                      'Font Size 6',
                      'fs-6'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_blineheight' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'Line Height',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      '1',
                      'lh-1'
                  ],
                  [
                      'Small',
                      'lh-sm'
                  ],
                  [
                      'Base',
                      'lh-base'
                  ],
                  [
                      'Large',
                      'lh-lg'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_bfontlayout' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'Color',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      'Primary',
                      'f-primary'
                  ],
                  [
                      'Primary Variant',
                      'f-primary-var'
                  ],
                  [
                      'Secondary',
                      'f-secondary'
                  ],
                  [
                      'Accent',
                      'f-accent'
                  ],
                  [
                      'White',
                      'f-white'
                  ],
                  [
                      'Light',
                      'f-light'
                  ],
                  [
                      'Dark',
                      'f-dark'
                  ],
                  [
                      'Base',
                      'f-base'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_htop' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'Margin top',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_ht_negative',
                      'ht-negative'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_ht_zero',
                      'ht-zero'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_ht_small',
                      'ht-small'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_ht_medium',
                      'ht-medium'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_ht_large',
                      'ht-large'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_ht_xlarge',
                      'ht-xlarge'
                  ]
              ]
          ],
        ],

        'tx_cogtail_wrap_hbottom' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'Margin bottom',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_hb_negative',
                      'hb-negative'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_hb_zero',
                      'hb-zero'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_hb_small',
                      'hb-small'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_hb_medium',
                      'hb-medium'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_hb_large',
                      'hb-large'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_hb_xlarge',
                      'hb-xlarge'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_class_textvw' => [
            'exclude' => 1,
            'displayCond' => 'FIELD:tx_cogtail_wrap_class_props:=:2',
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_adjustments',
            'description' => 'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_class_adjustments_desc',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectCheckBox',
              'items' => [
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_angle_bottom_left',
                      'angle--bottom-left'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_angle_bottom_right',
                      'angle--bottom-right'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_angle_top_left',
                      'angle--top-left'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_angle_top_right',
                      'angle--top-right'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_cta',
                      'cta'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_panel',
                      'panel'
                  ],
                  [
                      'Box for Textpic left or right',
                      'textpicbox'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_bshadow',
                      'box-shadow'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_mfull',
                      'make-fullwidth'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_mfullbg',
                      'make-fullwidth-bg'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_mtratio',
                      'mt-ratio'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_shiftq',
                      'shift-quotes'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_angle_fill2',
                      'angle-fill2'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_icongroup_fill2',
                      'icongroup-fill2'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_adjust_bg_card',
                      'adjust-bg-cards'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_d_block',
                      'd-block'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_autom',
                      'autom'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_lightgall',
                      'lightgall'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_zoomgall',
                      'zoomgall'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_alterlist',
                      'smart-list'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_remfeaturepad',
                    'rem-feat-padd'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_rembg',
                    'rem-bg'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_remerror',
                    'rem-err'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_fcolor',
                    'fcolor'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_wcolor',
                    'wcolor'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_lptsm',
                    'lptsm'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_lptmm',
                    'lptmm'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_lmlr',
                    'lmlr'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_lmlrtm',
                    'lmlrtm'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_lmlrm',
                    'lmlrm'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_mwpls',
                    'mwpls'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_class_properties' => [
            'exclude' => 1,
            'displayCond' => 'FIELD:tx_cogtail_wrap_class_props:=:1',
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_properties',
            'description' => 'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_class_properties_desc',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectCheckBox',
              'items' => [
                  [
                      'Border',
                      'border'
                  ],
                  [
                      'Box Shadow',
                      'box-shadow'
                  ],
                  [
                      'Rounded',
                      'rounded'
                  ],
                  [
                      'Display Block',
                      'd-block'
                  ],
                  [
                      'Display Inline',
                      'd-inline'
                  ],
                  [
                      'Center Horizontally',
                      'h-center'
                  ],
                  [
                      'Center Vertically',
                      'v-center'
                  ],
                  [
                      'Icon',
                      'icon'
                  ],
                  [
                    ':not',
                    'ps-not'
                  ],
                  [
                    'before',
                    'ps-before'
                  ],
                  [
                    'after',
                    'ps-after'
                  ],
                  [
                    ':first-of-type',
                    'first-of-type'
                  ],
                  [
                    ':last-of-type',
                    'last-of-type'
                  ],
                  [
                    'Padding',
                    'padding'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_bfontstyle' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_adjustments',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      'Lead',
                      'lead'
                  ],
                  [
                      'Uppercase',
                      'b-uppercase'
                  ],
                  [
                      'Normal',
                      'b-normal'
                  ],
                  [
                      'Letter Spacing',
                      'b-letter-spacing'
                  ],
                  [
                      'Small',
                      'b-small'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_textwidth' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_bodytext_width',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_cmwidthl',
                      'cmwidthl'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_cmwidthm',
                      'cmwidthm'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_cmwidths',
                      'cmwidths'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_cmwidthxs',
                      'cmwidthxs'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_galmobsiz' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_gallery_settings',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_galms1',
                      'gallery-mobile-size-1'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_galms3',
                    'gallery-no-padding'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_ob',
                    'over-box'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_gpb',
                    'gallery-padding-bottom'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_stacki0',
                    'stacked-0'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_stacki1',
                    'stacked-1'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_stacki2',
                    'stacked-2'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_stacki3',
                    'stacked-3'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_stacki4',
                    'stacked-4'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_gptms',
                    'gptms'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_gptmm',
                    'gptmm'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_bri',
                    'bri'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_cei',
                    'cei'
                  ],
                  [
                      'Thumbnail',
                      'thumbnail'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_objfit',
                    'objfit'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_imgwidth' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_grid',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      'Col 1',
                      'imgwidth-1'
                  ],
                  [
                    'Col 2',
                    'imgwidth-2'
                  ],
                  [
                    'Col 3',
                    'imgwidth-3'
                  ],
                  [
                    'Col 4',
                    'imgwidth-4'
                  ],
                  [
                    'Col 5',
                    'imgwidth-5'
                  ],
                  [
                    'Col 6',
                    'imgwidth-6'
                  ],
                  [
                    'Col 7',
                    'imgwidth-7'
                  ],
                  [
                    'Col 8',
                    'imgwidth-8'
                  ],
                  [
                    'Col 9',
                    'imgwidth-9'
                  ],
                  [
                    'Col 10',
                    'imgwidth-10'
                  ],
                  [
                    'Col 11',
                    'imgwidth-11'
                  ],
                  [
                    'Col 12',
                    'imgwidth-12'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_galads' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_spacing',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_gpbmnl',
                      'gpbmnl'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_gpbmnm',
                      'gpbmnm'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_gpbmns',
                      'gpbmns'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_gpbmz',
                      'gpbmz'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_gpbms',
                      'gpbms'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_gpbmm',
                    'gpbmm'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_gpbml',
                    'gpbml'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_gplrm',
                    'gplrm'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_galmobads' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_breakpoints',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectCheckBox',
              'items' => [
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_galms2',
                    'gallery-mobile-size-2'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_rotim',
                    'rotim'
                  ],
                  [
                    'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_ceim',
                    'cei-m'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_padtop' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'Padding top',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_espt',
                      'xsmall-padding-top'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_spt',
                      'small-padding-top'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_mpt',
                      'medium-padding-top'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_lpt',
                      'large-padding-top'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_xlpt',
                      'xlarge-padding-top'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_padleft' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'Padding left',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_xspl',
                      'xsmall-padding-left'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_spl',
                      'small-padding-left'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_mpl',
                      'medium-padding-left'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_lpl',
                      'large-padding-left'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_xlpl',
                      'xlarge-padding-left'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_padright' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'Padding right',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_xspr',
                      'xsmall-padding-right'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_spr',
                      'small-padding-right'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_mpr',
                      'medium-padding-right'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_lpr',
                      'large-padding-right'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_xlpr',
                      'xlarge-padding-right'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_cardcols' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'Card Columns',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      '1',
                      '1'
                  ],
                  [
                      '2',
                      '2'
                  ],
                  [
                      '3',
                      '3'
                  ],
                  [
                      '4',
                      '4'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_padbottom' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'Padding bottom',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_espb',
                      'xsmall-padding-bottom'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_spb',
                      'small-padding-bottom'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_mpb',
                      'medium-padding-bottom'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_lpb',
                      'large-padding-bottom'
                  ],
                  [
                      'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_xlpb',
                      'xlarge-padding-bottom'
                  ]
              ]
          ],
        ],
        'tx_cogtail_wrap_aos_input' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cogtail_wrap_class_scroll',
            'config' => [
              'type' => 'select',
              'renderType' => 'selectSingle',
              'items' => [
                  ['', 0],
                  [
                      'Reveal 1',
                      'reveal1'
                  ],
                  [
                      'Reveal 2',
                      'reveal2'
                  ],
                  [
                      'Reveal 3',
                      'reveal3'
                  ],
                  [
                      'Reveal 4',
                      'reveal4'
                  ]
              ]
          ],
        ],
    ];

    // Adding fields to the tt_content table definition in TCA
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns($table, $extraContentColumns);

    // Create palette

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes($table, '--palette--;LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:palette.cogtail_wrap;cogtail_wrap', '', 'after:layout');
    $GLOBALS['TCA'][$table]['palettes']['cogtail_wrap']['showitem'] = 'tx_cogtail_wrap_padleft,tx_cogtail_wrap_padright,tx_cogtail_wrap_padtop,tx_cogtail_wrap_padbottom, --linebreak--,tx_cogtail_wrap_textwidth,--linebreak--,tx_cogtail_wrap_class_props,--linebreak--,tx_cogtail_wrap_class_input,tx_cogtail_wrap_class_style,tx_cogtail_wrap_class_textvw,tx_cogtail_wrap_class_properties, --linebreak--, tx_cogtail_wrap_id_input,tx_cogtail_wrap_aos_input';
    // $GLOBALS['TCA'][$table]['palettes']['cogtail_wrap']['showitem'] = 'tx_cogtail_wrap_padtop,tx_cogtail_wrap_padbottom,tx_cogtail_wrap_textwidth,--linebreak--,tx_cogtail_wrap_class_input,tx_cogtail_wrap_class_textvw, --linebreak--, tx_cogtail_wrap_id_input,tx_cogtail_wrap_aos_input, image_responsive';

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes($table, 'topheader,additionalheader,--palette--;Header-Layout;cogtail_wrap2', '', 'after:header');
    $GLOBALS['TCA'][$table]['palettes']['cogtail_wrap2']['showitem'] = 'tx_cogtail_wrap_hstyles,tx_cogtail_wrap_hweight,tx_cogtail_wrap_hcolor,tx_cogtail_wrap_lheight,--linebreak--,tx_cogtail_wrap_htop,tx_cogtail_wrap_hbottom,tx_cogtail_wrap_hwidth,tx_cogtail_wrap_hdisplay,--linebreak--,tx_cogtail_wrap_hlayout,tx_cogtail_wrap_noheader';

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes($table, '--palette--;Column Adjustments;cogtail_wrap3', '', 'after:imagecols');
    $GLOBALS['TCA'][$table]['palettes']['cogtail_wrap3']['showitem'] = ' --linebreak--,tx_cogtail_wrap_galmobsiz,tx_cogtail_wrap_galads,tx_cogtail_wrap_imgwidth,--linebreak--,tx_cogtail_wrap_galmobads';

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes($table, '--palette--;LLL:EXT:cogtail_wrap/Resources/Private/Language/locallang.xlf:palette.cogtail_wrap;cogtail_wrap4', '', 'after:bodytext');
    $GLOBALS['TCA'][$table]['palettes']['cogtail_wrap4']['showitem'] = 'tx_cogtail_wrap_bfontstyle,tx_cogtail_wrap_bfontsize,tx_cogtail_wrap_blineheight,tx_cogtail_wrap_bfontlayout';

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes($table, '--palette--;Column Adjustments;menu_categorized_content', '', 'after:selected_categories');
    $GLOBALS['TCA'][$table]['palettes']['menu_categorized_content']['showitem'] = ' --linebreak--,tx_cogtail_wrap_cardcols,--linebreak--';


})('tt_content');
