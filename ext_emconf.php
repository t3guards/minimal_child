<?php

/**
 * Extension Manager/Repository config file for ext "minimal_child".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Minimal Child',
    'description' => '',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-12.5.99',
            'fluid_styled_content' => '11.5.0-12.5.99',
            'rte_ckeditor' => '11.5.0-12.5.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'T3guards\\MinimalChild\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'René Gast',
    'author_email' => 'kontakt@t3guards.de',
    'author_company' => 'T3Guards',
    'version' => '1.0.0',
];
