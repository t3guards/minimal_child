CREATE TABLE sys_file_reference (
   gallerysize varchar(15) DEFAULT '' NOT NULL,
   mediaclass varchar(15) DEFAULT '' NOT NULL
);

#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
  image_responsive smallint unsigned DEFAULT '0' NOT NULL,
	tx_cogtail_wrap_id_input varchar(255) DEFAULT '' NOT NULL,
	tx_cogtail_wrap_class_input varchar(255) DEFAULT '' NOT NULL,
	tx_cogtail_wrap_noheader varchar(255) DEFAULT '' NOT NULL,
	tx_cogtail_wrap_class_textvw varchar(255) DEFAULT '' NOT NULL,
  tx_cogtail_wrap_class_style varchar(255) DEFAULT '' NOT NULL,
  tx_cogtail_wrap_class_props varchar(255) DEFAULT '' NOT NULL,
  tx_cogtail_wrap_class_properties varchar(255) DEFAULT '' NOT NULL,
  tx_cogtail_wrap_bfontstyle varchar(255) DEFAULT '' NOT NULL,
  tx_cogtail_wrap_bfontsize varchar(255) DEFAULT '' NOT NULL,
  tx_cogtail_wrap_bfontlayout varchar(255) DEFAULT '' NOT NULL,
  tx_cogtail_wrap_blineheight varchar(255) DEFAULT '' NOT NULL,
	tx_cogtail_wrap_aos_input varchar(255) DEFAULT '' NOT NULL,
  tx_cogtail_wrap_icons varchar(255) DEFAULT '0' NOT NULL,
	tx_cogtail_wrap_hstyles varchar(255) DEFAULT '0' NOT NULL,
  tx_cogtail_wrap_hweight varchar(255) DEFAULT '0' NOT NULL,
  tx_cogtail_wrap_hcolor varchar(255) DEFAULT '0' NOT NULL,
	tx_cogtail_wrap_input30 text,
	tx_cogtail_wrap_hheight text,
	tx_cogtail_wrap_lheight varchar(255) DEFAULT '0' NOT NULL,
	tx_cogtail_wrap_hdisplay varchar(255) DEFAULT '0' NOT NULL,
  tx_cogtail_wrap_hlayout varchar(255) DEFAULT '0' NOT NULL,
  tx_cogtail_wrap_hwidth varchar(255) DEFAULT '0' NOT NULL,
  tx_cogtail_wrap_htop varchar(255) DEFAULT '0' NOT NULL,
	tx_cogtail_wrap_hbottom varchar(255) DEFAULT '0' NOT NULL,
	tx_cogtail_wrap_padtop varchar(255) DEFAULT '0' NOT NULL,
  tx_cogtail_wrap_padleft varchar(255) DEFAULT '0' NOT NULL,
  tx_cogtail_wrap_padright varchar(255) DEFAULT '0' NOT NULL,
	tx_cogtail_wrap_padbottom varchar(255) DEFAULT '0' NOT NULL,
	tx_cogtail_wrap_galmobsiz varchar(255) DEFAULT '0' NOT NULL,
	tx_cogtail_wrap_galmobads varchar(255) DEFAULT '0' NOT NULL,
	tx_cogtail_wrap_galads varchar(255) DEFAULT '0' NOT NULL,
	tx_cogtail_wrap_textwidth varchar(255) DEFAULT '0' NOT NULL,
  tx_cogtail_wrap_imgwidth varchar(255) DEFAULT '0' NOT NULL,
	topheader varchar(255) DEFAULT '' NOT NULL,
  tx_cogtail_wrap_cardcols varchar(255) DEFAULT '0' NOT NULL,
  additionalheader varchar(255) DEFAULT '' NOT NULL,

);

CREATE TABLE pages (
	tx_cogtail_wrap_fullw varchar(255) DEFAULT '' NOT NULL,
	tx_cogtail_wrap_fullw2 varchar(255) DEFAULT '' NOT NULL,
	tx_cogtail_wrap_fp varchar(255) DEFAULT '' NOT NULL
);
